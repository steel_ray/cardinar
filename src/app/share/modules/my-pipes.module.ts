import { NgModule } from '@angular/core';
import { MessagePipe } from '../pipes/message.pipe';
import { SplitString } from '../pipes/split-string.pipe';
import { SafeHtmlPipe } from '../pipes/safe.html';

@NgModule({
  declarations: [MessagePipe, SplitString, SafeHtmlPipe],
  exports: [MessagePipe, SplitString, SafeHtmlPipe]
})
export class MyPipesModule {}
