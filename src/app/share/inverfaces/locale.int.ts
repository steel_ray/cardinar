export interface Locale {
  locale: String;
  name: String;
}

export interface LocaleResponse {
  status: Number;
  error: Array<any>;
  data: Array<Locale>;
}
