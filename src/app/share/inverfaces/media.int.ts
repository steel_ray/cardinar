export interface Media {
  filename: String;
  type: String;
  url: String;
  size: Number;
  title: String;
  alt: String;
  description: String;
  album_title: String;
  thumbnail: String;
}

export interface MediaResponse {
  data: Array<Media>;
  status: Number;
  error: any;
}
