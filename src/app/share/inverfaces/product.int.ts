export interface Category {
  title: string;
  slug: string;
  images: {
    img1: string;
    img2: string;
    img3: string;
  };
}
export interface Texture {
  id: number;
  filename: string;
  chosen: number;
  product: { slug: string; title: string };
}
export interface Color {
  id: number;
  title: string;
  color: string;
}
export interface Product {
  title: string;
  price: number;
  slug: string;
  new: number;
  thumbnail: string;
  main_img: string;
  category: Category;
  id: number;
  vendor_code: string;
  images: {
    img1: string;
    img2: string;
    img3: string;
    main_img: string;
  };
  color?: Array<Color>;
  textures?: Array<Texture>;
}

export interface ProductResponse {
  data: Array<Product>;
  status: number;
  error: any;
}
export interface CategoryResponse {
  data: Array<Category>;
  status: number;
  error: any;
}
export interface TextureResponse {
  data: Array<Texture>;
  status: number;
  error: any;
}
