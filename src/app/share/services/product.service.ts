import { Injectable } from '@angular/core';
import { ApiBase } from '../../base/api-base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product, Category, Texture } from '../inverfaces/product.int';

@Injectable()
export class ProductService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }
  getProducts(
    params: {
      category_slug?: string;
      order_rand?: boolean;
      limit?: number;
      expand?: string;
      skip?: string;
      new?: number;
      popular?: number;
      page?: number;
    } = {},
    observe: string = null
  ): Observable<Product[]> {
    return this.get('/products', params, observe);
  }
  getProduct(slug: string): Observable<Product> {
    return this.get(`/products/${slug}`);
  }
  getCategories(): Observable<Category[]> {
    return this.get('/categories');
  }
  getTextures(
    params: {
      product_slug?: string;
      order_rand?: boolean;
      limit?: number;
      expand?: string;
    } = {}
  ): Observable<Texture[]> {
    return this.get('/textures', params);
  }
}
