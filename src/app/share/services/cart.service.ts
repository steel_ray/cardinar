import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Cart } from '../models/cart.model';
import { Helpers } from '../../base/helpers';

@Injectable()
export class CartService extends Helpers {
  private emitChangeSource = new Subject();
  changeEmited = this.emitChangeSource.asObservable();
  emitChange(number: number) {
    this.emitChangeSource.next(number);
  }

  addToCart(product: Cart) {
    const cart: Cart[] = this.cartItems;
    cart.push(product);
    localStorage.setItem('cart', JSON.stringify(cart));
    this.shareCartCount();
  }
  removeFromCart(id) {
    const cart = this.cartItems;
    const index = cart.findIndex(p => p.id === id);
    if (index !== -1) {
      cart.splice(index, 1);
    }
    if (!cart.length) {
      localStorage.removeItem('cart');
    } else {
      localStorage.setItem('cart', JSON.stringify(cart));
    }
    this.shareCartCount();
  }
  get installationCost() {
    return +this.getSetting('installation_cost');
  }
  get cartItems() {
    const cart = JSON.parse(localStorage.getItem('cart'));
    return !cart ? [] : cart;
  }
  get totalQuantity() {
    let quantity = 0;
    this.cartItems.map(item => {
      quantity += item.quantity;
    });
    return quantity;
  }
  get totalPrice() {
    let price = 0;
    this.cartItems.map(item => {
      price += item.quantity * item.price;
      if (item.installation) {
        price += item.quantity * this.installationCost;
      }
    });
    return price;
  }
  shareCartCount(): void {
    this.emitChange(this.cartItems.length);
  }
  checkCart(id: number) {
    const cart: Cart[] = this.cartItems;
    if (cart) {
      return cart.find(p => p.id === id) ? true : false;
    }
    return false;
  }
  emptyCart() {
    localStorage.removeItem('cart');
    this.shareCartCount();
  }
}
