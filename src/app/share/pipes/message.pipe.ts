import { Pipe, PipeTransform } from '@angular/core';
import { Helpers } from '../../base/helpers';

@Pipe({
  name: 'getMessage'
})

export class MessagePipe extends Helpers implements PipeTransform {
  transform(value: string) {
    return this.getMessage(value);
  }
}
