import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiBase {
  APIEndpoint = environment.APIEndpoint;
  constructor(public http: HttpClient) {}

  public get(url: string, params: any = {}, observe = null): Observable<any> {
    params['lang'] = localStorage.getItem('locale');
    return this.http.get(this.APIEndpoint + url, { params, observe }).pipe(
      map(res => res),
      catchError(this.handleError)
    );
  }
  public post(url: string, data: any, headers: any = null): Observable<any> {
    return this.http
      .post(this.APIEndpoint + url, data, { headers: headers })
      .pipe(
        map(res => res),
        catchError(this.handleError)
      );
  }

  public put(url: string, data: any, headers: any): Observable<any> {
    return this.http
      .put(this.APIEndpoint + url, data, { headers: headers })
      .pipe(
        map(res => res),
        catchError(this.handleError)
      );
  }
  private handleError(error: any) {
    if (error.error && error.error.message) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        error
        // `Backend returned code ${error.status}, ` +
        // `body was: ${error.error.toString()}`
      );
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
  public urlParams(params: {}) {
    let res = '';
    const keys = Object.keys(params);
    if (keys.length) {
      const urlParams = new URLSearchParams();
      keys.forEach(key => {
        urlParams.set(key, params[key]);
      });
      res = '&' + urlParams.toString();
    }
    return res;
  }
  public serialize(obj) {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  }
}
