export class Helpers {
  getMessage(key: string) {
    const messages = this.getSetting('messages');
    if (messages && Object.keys(messages).includes(key) && messages[`${key}`]) {
      return messages[`${key}`];
    }
    return key;
  }

  getSetting(key: string) {
    const settings = JSON.parse(localStorage.getItem('settings'));
    return settings && settings[key] ? settings[key] : null;
  }

  splitString(str: string) {
    const arr = str.split('');
    let title = '';
    arr.forEach(key => {
      let className = null;
      if (key === ' ') {
        key = '&nbsp;';
        className = 'class="space"';
      }
      title += `<span ${className}>${key}</span>`;
    });
    return title;
  }

  chunkArray(myArray: any, chunk_size: number) {
    const results = [];

    while (myArray.length) {
      results.push(myArray.splice(0, chunk_size));
    }

    return results;
  }

  isHomeCheck(url: string) {
    if (url === '/') {
      return true;
    } else {
      return false;
    }
  }
}
