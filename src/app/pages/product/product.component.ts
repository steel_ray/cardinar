import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product, Category } from '../../share/inverfaces/product.int';
import { Subscription, Observable, combineLatest } from 'rxjs';
import { ProductService } from '../../share/services/product.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HeaderImagesService } from '../../share/services/header-images.service';
import { Cart } from '../../share/models/cart.model';
import { CartService } from '../../share/services/cart.service';

@Component({
  selector: 'ln-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  product: Product;
  slug = '';
  sub: Subscription;
  routeSub: Subscription;
  similarProducts: Observable<Array<Product>>;
  categories: Category[];
  categorySlug: string;
  quantity = 1;
  price: number;
  addedToCart = false;
  installation = false;
  isLoaded = false;
  isLoading = false;
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private headerImagesService: HeaderImagesService,
    private cartService: CartService
  ) {}

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.slug = params['slug'];
    });
    this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.loadData();
      }
    });
    if (!this.isLoading) {
      this.loadData();
    }
  }

  loadData() {
    this.isLoading = true;
    const combined = combineLatest(
      this.productService.getProduct(this.slug),
      this.productService.getCategories()
    );

    this.sub = combined.subscribe(res => {
      this.product = res[0];
      this.categories = res[1];
      this.categorySlug = this.product.category.slug;
      this.price = this.product.price;
      this.addedToCart = this.cartService.checkCart(this.product.id);
      this.countQuantityPriceFromCart(this.product.id);
      this.similarProducts = this.productService.getProducts({
        category_slug: this.product.category.slug,
        skip: this.product.slug,
        limit: 3,
        order_rand: true
      });
      this.isLoaded = true;
      // tslint:disable-next-line:max-line-length
      // setTimeout to avoid debug error like "ERROR Error: "ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked"
      setTimeout(() => {
        this.headerImagesService.emitChange(
          this.product.category.images ? this.product.category.images : null
        );
      });
    });
  }

  initChange(e) {
    this.installation = e.checked;
    const items = this.cartService.cartItems.map(item => {
      if (item.id === this.product.id) {
        item.installation = this.installation;
      }
      return item;
    });
    localStorage.setItem('cart', JSON.stringify(items));
  }

  onFilterChange(e) {
    this.router.navigate(['/catalog', e.value]);
  }

  countQuantityPriceFromCart(id): void {
    if (this.addedToCart) {
      const item = this.cartService.cartItems.find(p => p.id === id);
      this.quantity = item.quantity;
      this.installation = item.installation;
      this.price = this.quantity * this.product.price;
    }
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
  goToCatalog(slug: string): void {
    slug = slug === 'all' ? '' : slug;
    this.router.navigate(['/catalog', slug], { relativeTo: this.route });
  }

  setQuantity(increase = true) {
    if (this.quantity === 1 && !increase) {
      return false;
    }
    this.quantity = increase ? this.quantity + 1 : this.quantity - 1;
    this.price = this.quantity * this.product.price;
    const items = this.cartService.cartItems.map(item => {
      if (item.id === this.product.id) {
        item.quantity = this.quantity;
      }
      return item;
    });
    localStorage.setItem('cart', JSON.stringify(items));
  }
  order(id: number) {
    const product: Cart = {
      id: this.product.id,
      slug: this.product.slug,
      title: this.product.title,
      vendor_code: this.product.vendor_code,
      price: this.product.price,
      quantity: this.quantity,
      installation: this.installation
    };
    if (this.product.thumbnail) {
      product.thumbnail = this.product.thumbnail;
    }
    this.addedToCart = true;
    this.cartService.addToCart(product);
  }
  cancelOrder(id) {
    this.cartService.removeFromCart(id);
    this.addedToCart = false;
    this.quantity = 1;
    this.price = this.product.price;
  }
}
