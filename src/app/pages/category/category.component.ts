import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { PostService } from '../../share/services/post.service';
import { Post } from '../../share/inverfaces/post.int';

@Component({
  selector: 'ln-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  slug: string;
  sub: Subscription;
  routerSub: Subscription;
  isLoaded = false;
  isLoading = false;
  posts: Post[];
  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private router: Router
  ) {}

  ngOnInit() {
    this.routerSub = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.loadData();
      }
    });
    this.route.params.subscribe(res => {
      this.slug = res['slug'];
    });
    if (!this.isLoading) {
      this.loadData();
    }
  }
  loadData(): void {
    this.isLoading = true;
    this.sub = this.postService
      .getPosts({ category_slug: this.slug })
      .subscribe(res => {
        this.isLoaded = true;
        this.posts = res;
        if (!this.posts.length) {
          this.router.navigate(['error-page']);
        }
      });
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    this.isLoaded = false;
  }
}
