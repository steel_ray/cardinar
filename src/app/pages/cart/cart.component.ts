import { Component, OnInit } from '@angular/core';
import { CartService } from '../../share/services/cart.service';
import { Cart } from '../../share/models/cart.model';

@Component({
  selector: 'ln-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  items: Cart[];
  totalQuantity: number;
  totalPrice: number;
  installationCost: number;
  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.items = this.cartService.cartItems;
    this.totalQuantity = this.cartService.totalQuantity;
    this.totalPrice = this.cartService.totalPrice;
    this.installationCost = this.cartService.installationCost;
    // console.log(this.installationCost);
  }

  initChange(e, id: number) {
    const items = this.cartService.cartItems.map(item => {
      if (item.id === id) {
        item.installation = e.checked;
      }
      return item;
    });
    localStorage.setItem('cart', JSON.stringify(items));
    this.items = this.cartService.cartItems;
    this.totalPrice = this.cartService.totalPrice;
  }

  setQuantity(increase = true, id: number) {
    this.items.map(item => {
      if (increase) {
        return item.id === id ? (item.quantity = item.quantity + 1) : item;
      } else if (item.quantity > 1) {
        return item.id === id ? (item.quantity = item.quantity - 1) : item;
      }
    });
    localStorage.setItem('cart', JSON.stringify(this.items));
    this.totalPrice = this.cartService.totalPrice;
    this.totalQuantity = this.cartService.totalQuantity;
  }
  removeFromCart(id: number) {
    this.cartService.removeFromCart(id);
    this.items = this.cartService.cartItems;
    this.totalPrice = this.cartService.totalPrice;
    this.totalQuantity = this.cartService.totalQuantity;
  }
}
