import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { Helpers } from '../base/helpers';
import { Post } from '../share/inverfaces/post.int';
import { PagesService } from './pages.service';
import { Product } from '../share/inverfaces/product.int';

@Component({
  selector: 'ln-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent extends Helpers implements OnInit {
  sub: Subscription;
  reviews$: Observable<Post[]>;
  newProducts$: Observable<Product[]>;
  popularProducts$: Observable<Product[]>;
  isLoaded = false;
  isHome = true;
  routePath = '';
  routerSub: Subscription;
  newProductsOption = {
    margin: 75,
    nav: false,
    dots: false,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    // lazyLoad: true,
    smartSpeed: 1200,
    // slideTransition: 4000,
    // navText: [
    //   '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
    //   '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    // ],
    responsiveClass: true,
    autoplaySpeed: 1200,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 80
      },
      1500: {
        items: 4,
        nav: true,
        loop: false
      }
    }
    // onChanged: function(event) {
    //   console.log(event);
    //   // const items = this.elRef.nativeElement.querySelectorAll('.production-list .owl-item.active');
    //   // console.log(items);
    // }
  };
  reviewsCarouselOptions = {
    margin: 0,
    nav: true,
    dots: false,
    autoplayTimeout: 6000,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 1200,
    autoplaySpeed: 1200,
    // navText: [
    //   '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
    //   '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    // ],
    responsiveClass: true,
    // autoWidth: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 1,
        nav: true,
        loop: false
      },
      1500: {
        items: 1,
        nav: true,
        loop: false
      }
    }
  };
  popularProductsCarouselOptions = {
    margin: 0,
    nav: true,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 800,
    autoplaySpeed: 800,
    // navText: [
    //   '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
    //   '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    // ],
    responsiveClass: true,
    // autoWidth: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
        loop: false
      },
      1500: {
        items: 3,
        nav: true,
        loop: false
      }
    },
    onInitialized: () => {
      this.findCentralItem();
    },
    onTranslate: () => {
      this.findCentralItem(true);
    },
    onTranslated: () => {
      this.findCentralItem();
    }
  };

  findCentralItem(remove = false): void {
    const activeItems = this.elRef.nativeElement.querySelectorAll(
      '.pp-slider .owl-item.active'
    );
    if (activeItems.length > 2) {
      activeItems.forEach(item => {
        if (remove) {
          item.classList.remove('central');
          item.classList.add('animating');
        } else {
          item.classList.remove('animating');
          if (item !== activeItems[1]) {
            item.classList.remove('central');
          } else {
            item.classList.add('central');
          }
        }
      });
    }
  }

  constructor(
    private pageService: PagesService,
    private router: Router,
    private elRef: ElementRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.isHome = this.isHomeCheck(this.router.url);
    this.routerSub = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.isHome = this.isHomeCheck(this.router.url);
      }
    });
    this.routePath = this.router.url;
    this.reviews$ = this.pageService.reviews;
    this.newProducts$ = this.pageService.newProducts;
    this.popularProducts$ = this.pageService.popularProducts;
  }
}
