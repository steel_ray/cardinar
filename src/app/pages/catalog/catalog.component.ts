import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Product, Category } from '../../share/inverfaces/product.int';
import { Subscription } from 'rxjs';
import { CatalogService } from './catalog.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Helpers } from '../../base/helpers';
import { Location } from '@angular/common';
import { DeviceDetectorService } from 'ngx-device-detector';
@Component({
  selector: 'ln-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent extends Helpers implements OnInit, OnDestroy {
  products: Product[];
  categories: Array<Category>;
  categoriesGroup: any;
  sub: Subscription;
  sub2: Subscription;
  routeSub: Subscription;
  isLoaded = false;
  slug = 'all';
  limit = 9;
  limits = [9, 18, 30, 50];
  page = 1;
  totalCount: number;
  viewType = 'grid'; // list
  currentGroup = 0;
  sort = 'all';
  redirecting = false;
  q: string;
  chunkArraySize = 5;
  catalogCatsOption = {
    margin: 60,
    nav: false,
    dots: false,
    autoplay: false,
    mouseDrag: false,
    touchDrag: false,
    rewind: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false,
        margin: 80
      },
      1500: {
        items: 5,
        nav: true,
        loop: false
      }
    }
  };
  constructor(
    private catalogService: CatalogService,
    private route: ActivatedRoute,
    private elref: ElementRef,
    private router: Router,
    private location: Location,
    private deviceService: DeviceDetectorService
  ) {
    super();
  }

  ngOnInit(): void {
    const isMobile = this.deviceService.isMobile();
    // console.log(isMobile);
    if (isMobile) {
      this.viewType = 'list';
    }
    this.catalogService.categories.subscribe(res => {
      this.categories = res;
      this.categoriesGroup = this.chunkArray(
        [...this.categories],
        this.chunkArraySize
      );
    });
    if (!this.isLoaded) {
      this.slug = this.route.snapshot.paramMap.get('slug');
      const paramsMap = this.route.snapshot.queryParamMap;
      this.page = +paramsMap.get('page');
      this.limit = +paramsMap.get('limit')
        ? +paramsMap.get('limit')
        : this.limit;
      this.sort = paramsMap.get('sort');
      this.q = paramsMap.get('q');
      this.loadData();
    }
    this.sub2 = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.loadData();
      }
    });
  }

  loadData() {
    this.isLoaded = false;
    this.sub = this.catalogService.products(this.queryParams).subscribe(res => {
      this.products = res['body'];
      // console.log(res[1]['headers'].headers);
      this.totalCount = res['headers'].get('X-Pagination-Total-Count');
      if (this.slug !== 'all') {
        let i = 0;
        if (this.categoriesGroup) {
          this.categoriesGroup.forEach(arr => {
            if (arr.find(c => c.slug === this.slug)) {
              this.currentGroup = i;
            }
            i++;
          });
        }
      }
      this.isLoaded = true;
      setTimeout(() => {
        this.setCatsSliderHeight();
        this.gridDetailsShow();
      }, 100);
    });
  }

  goToPage(e: any): void {
    this.page = e.pageIndex;
    // console.log(e, this.page);
    this.redirect();
  }

  filter(e): void {
    this.sort = e.value;
    this.redirect();
  }
  changeLimit(e) {
    this.limit = e.value;
    this.redirect();
  }
  goToCat(slug: string) {
    this.slug = slug;
    this.redirect();
  }

  redirect(): void {
    this.loadData();
    const params = this.queryParams;
    if (params['category_slug']) {
      delete params['category_slug'];
    }
    const paramsString = this.urlParams(params);
    const url = this.slug
      ? `/${this.slug}?${paramsString}`
      : `?${paramsString}`;
    this.location.replaceState(`/catalog${url}`);
  }

  urlParams(params: {}) {
    let res = '';
    const keys = Object.keys(params);
    if (keys.length) {
      const urlParams = new URLSearchParams();
      keys.forEach(key => {
        urlParams.set(key, params[key]);
      });
      res = urlParams.toString();
    }
    return res;
  }

  get queryParams() {
    const params = {
      limit: this.limit
    };
    if (this.page) {
      params['page'] = this.page;
    }
    if (this.sort) {
      params['sort'] = this.sort;
    }
    if (this.slug && !this.redirecting) {
      params['category_slug'] = this.slug;
    }
    if (this.q) {
      params['q'] = this.q;
    }
    return params;
  }

  changeView(e: any) {
    this.viewType = e.target.dataset['view'];
  }

  setCatsSliderHeight() {
    const slider = this.elref.nativeElement.querySelector(
      '.catalog-cats-slider'
    );
    const active = this.elref.nativeElement.querySelector(
      '.catalog-cats-slider .active'
    );
    if (active !== 'undefined' && active) {
      const height = active.clientHeight;
      slider.style.height = `${height}px`;
    }
  }
  showCats(): void {
    this.currentGroup =
      this.currentGroup >= this.categoriesGroup.length - 1
        ? 0
        : this.currentGroup + 1;
    setTimeout(() => {
      this.setCatsSliderHeight();
    }, 100);
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.sub2) {
      this.sub.unsubscribe();
    }
  }
  gridDetailsShow() {
    const items = this.elref.nativeElement.querySelectorAll('.clp-item');
    items.forEach((item: any) => {
      item.addEventListener('mouseenter', function(e) {
        const windowHeight = window.innerHeight;
        const windowScrollY = window.scrollY;
        const rect = item.getBoundingClientRect();
        let className: string;
        if (windowHeight - rect.bottom < rect.top) {
          className = 'top';
        } else {
          className = 'bottom';
        }
        e.currentTarget.classList.add(className);
      });
      item.addEventListener('mouseleave', function(e: any) {
        e.currentTarget.classList.remove('top');
        e.currentTarget.classList.remove('bottom');
      });
    });
  }
}
