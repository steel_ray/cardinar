import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../../share/inverfaces/product.int';
import { ProductService } from '../../share/services/product.service';
import { shareReplay } from 'rxjs/operators';

const CACHE_SIZE = 1;

@Injectable()
export class CatalogService {
  categoriesCache: Observable<Array<Category>>;
  constructor(private productService: ProductService) {}

  products(
    params: {
      limit: number;
      page?: number;
      category_slug?: string;
      sort?: string;
      expand?: 'category';
    } = { limit: 10, page: 1 }
  ) {
    return this.productService.getProducts(params, 'response');
  }

  get categories() {
    if (!this.categoriesCache) {
      this.categoriesCache = this.categoriesRequest().pipe(
        shareReplay(CACHE_SIZE)
      );
    }
    return this.categoriesCache;
  }
  categoriesRequest(): Observable<any> {
    return this.productService.getCategories();
  }
}
