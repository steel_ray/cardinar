import { Injectable } from '@angular/core';
import { ApiBase } from '../../base/api-base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class OrderService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }

  sendOrder(data: {}): Observable<any> {
    return this.post('/order', data);
  }
}
