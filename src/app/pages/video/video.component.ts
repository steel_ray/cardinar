import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ApiBase } from '../../base/api-base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Videos {
  title: string;
  video: string;
  source: boolean;
  description: string;
}

@Component({
  selector: 'ln-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VideoComponent extends ApiBase implements OnInit {
  videos: Observable<Videos[]>;
  constructor(public http: HttpClient) {
    super(http);
  }

  ngOnInit() {
    this.videos = this.get('videos').pipe(map(res => res['data']));
  }
}
