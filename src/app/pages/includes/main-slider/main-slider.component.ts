import { Component, ViewEncapsulation, Input, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Media } from '../../../share/inverfaces/media.int';
@Component({
  selector: 'ln-main-slider',
  templateUrl: './main-slider.component.html',
  styleUrls: ['./main-slider.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainSliderComponent {
  @Input() slides: Media[];
  slidesCarouselOptions = {
    margin: 0,
    nav: true,
    dots: true,
    autoplayTimeout: 4000,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 1200,
    autoplaySpeed: 1200,
    // navText: [
    //   '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
    //   '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    // ],
    responsiveClass: true,
    // autoWidth: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 1,
        nav: true,
        loop: false
      },
      1500: {
        items: 1,
        nav: true,
        loop: false
      }
    },
    onTranslate: () => {
      this.sliderItems(true);
    },
    onTranslated: () => {
      this.sliderItems();
    }
  };
  sliderItems(animating = false): void {
    const activeItems = this.elref.nativeElement.querySelectorAll(
      '.main-slider .owl-item'
    );
    activeItems.forEach(item => {
      if (animating) {
        item.classList.remove('animated');
        item.classList.add('animating');
      } else {
        item.classList.add('animated');
        item.classList.remove('animating');
      }
    });
  }
  constructor(private elref: ElementRef) {}
}
