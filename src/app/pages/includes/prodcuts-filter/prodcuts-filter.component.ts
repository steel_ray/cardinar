import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Category } from '../../../share/inverfaces/product.int';

@Component({
  selector: 'ln-prodcuts-filter',
  templateUrl: './prodcuts-filter.component.html',
  styleUrls: ['./prodcuts-filter.component.scss']
})
export class ProdcutsFilterComponent {
  @Output() selectedCategory = new EventEmitter<Category>();
  @Input() categories: Category[];
  @Input() slug: string;

  onFilterChange(e) {
    this.selectedCategory.emit(e.value);
  }
}
