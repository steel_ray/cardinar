import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdcutsFilterComponent } from './prodcuts-filter.component';

describe('ProdcutsFilterComponent', () => {
  let component: ProdcutsFilterComponent;
  let fixture: ComponentFixture<ProdcutsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdcutsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdcutsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
