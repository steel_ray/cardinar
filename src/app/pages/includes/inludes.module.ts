import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { ShareModule } from '../../share/modules/share.module';
import { MenuService } from './menu/menu.service';
import { MenuLinkPipe } from './menu/menu-link.pipe';
import { PreloaderComponent } from './preloader/preloader.component';
import { PostService } from '../../share/services/post.service';
import { ProductService } from '../../share/services/product.service';
import { MediaService } from '../../share/services/media.service';
import { FeedbackComponent } from './feedback/feedback.component';
import { FeedbackService } from './feedback/feedback.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProdcutsFilterComponent } from './prodcuts-filter/prodcuts-filter.component';
import { MainSliderComponent } from './main-slider/main-slider.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    MenuLinkPipe,
    PreloaderComponent,
    FeedbackComponent,
    ProdcutsFilterComponent,
    MainSliderComponent
  ],
  imports: [ShareModule, ReactiveFormsModule, FormsModule],
  exports: [
    ShareModule,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    PreloaderComponent,
    ProdcutsFilterComponent,
    ReactiveFormsModule,
    FormsModule,
    MainSliderComponent
  ],
  providers: [
    MenuService,
    PostService,
    ProductService,
    MediaService,
    FeedbackService
  ]
})
export class IncludesModule {}
