import { Component } from '@angular/core';

@Component({
  selector: 'ln-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.scss']
})
export class PreloaderComponent {}
