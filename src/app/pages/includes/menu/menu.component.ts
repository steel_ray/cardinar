import { Component, Input } from '@angular/core';
import { MenuModel } from './menu.model';

@Component({
  selector: 'ln-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  @Input() menu: MenuModel;
}
