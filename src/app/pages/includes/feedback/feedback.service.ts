import { Injectable } from '@angular/core';
import { ApiBase } from '../../../base/api-base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Feedback, FeedbackResponse } from './feedback.int';

@Injectable()
export class FeedbackService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }
  send(data: Feedback): Observable<FeedbackResponse> {
    return this.post('/feedback', data);
  }
}
