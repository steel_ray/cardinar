import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { HomeService } from './home.service';
import { combineLatest } from 'rxjs';
import { Product, Category } from '../../share/inverfaces/product.int';
import { Media } from '../../share/inverfaces/media.int';

@Component({
  selector: 'ln-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  isLoaded = false;
  slides: Media[];
  sub: Subscription;
  products: Array<Product>;
  categories: Array<Category>;
  catalogCatsOption = {
    margin: 60,
    nav: true,
    dots: false,
    autoplay: false,
    rewind: true,
    // autoplayHoverPause: true,
    // lazyLoad: true,
    // smartSpeed: 1200,
    // slideTransition: 4000,
    // navText: [
    //   '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
    //   '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    // ],
    // responsiveClass: true,
    // autoplaySpeed: 1200,
    responsive: {
      0: {
        items: 3,
        nav: true
      },
      600: {
        items: 3,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false,
        margin: 80
      },
      1500: {
        items: 5,
        nav: true,
        loop: false
      }
    }
    // onChanged: function(event) {
    //   console.log(event);
    //   // const items = this.elRef.nativeElement.querySelectorAll('.production-list .owl-item.active');
    //   // console.log(items);
    // }
  };
  catalogOption = {
    margin: 74,
    nav: false,
    dots: false,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    // lazyLoad: true,
    smartSpeed: 1200,
    // slideTransition: 4000,
    // navText: [
    //   '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
    //   '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    // ],
    responsiveClass: true,
    autoplaySpeed: 1200,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 80
      },
      1500: {
        items: 3,
        nav: true,
        loop: false
      }
    }
    // onChanged: function(event) {
    //   console.log(event);
    //   // const items = this.elRef.nativeElement.querySelectorAll('.production-list .owl-item.active');
    //   // console.log(items);
    // }
  };

  constructor(private homeService: HomeService, private elRef: ElementRef) {}

  ngOnInit(): void {
    const combined = combineLatest(
      this.homeService.slides,
      this.homeService.products,
      this.homeService.categories
    );
    this.sub = combined.subscribe(res => {
      this.slides = res[0]['data'];
      this.products = res[1];
      this.categories = res[2];
      this.isLoaded = true;
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
