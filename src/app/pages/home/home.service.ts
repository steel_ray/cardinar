import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ProductService } from '../../share/services/product.service';
import { Product, Category } from '../../share/inverfaces/product.int';
import { MediaService } from '../../share/services/media.service';
import { Media } from '../../share/inverfaces/media.int';

const CACHE_SIZE = 1;

@Injectable()
export class HomeService {
  slidesCache: Observable<Array<Media>>;
  productsCache: Observable<Array<Product>>;
  categoriesCache: Observable<Array<Category>>;
  constructor(
    private productService: ProductService,
    private mediaService: MediaService
  ) {}
  get slides() {
    if (!this.slidesCache) {
      this.slidesCache = this.mediaService
        .getAlbum('main-slider')
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.slidesCache;
  }

  get products() {
    if (!this.productsCache) {
      this.productsCache = this.productService
        .getProducts({ expand: 'category' })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.productsCache;
  }
  get categories() {
    if (!this.categoriesCache) {
      this.categoriesCache = this.productService
        .getCategories()
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.categoriesCache;
  }
}
