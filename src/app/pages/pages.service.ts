import { Injectable } from '@angular/core';
import { PostService } from '../share/services/post.service';
import { Observable } from 'rxjs';
import { Post } from '../share/inverfaces/post.int';
import { shareReplay } from 'rxjs/operators';
import { ProductService } from '../share/services/product.service';
import { Product } from '../share/inverfaces/product.int';
const CACHE_SIZE = 1;
@Injectable()
export class PagesService {
  reviewsCache: Observable<Post[]>;
  newProductsCache: Observable<Product[]>;
  popularProductsCache: Observable<Product[]>;
  constructor(
    private postService: PostService,
    private productService: ProductService
  ) {}

  get newProducts() {
    if (!this.newProductsCache) {
      this.newProductsCache = this.productService
        .getProducts({ limit: 20, new: 1 })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.newProductsCache;
  }
  get popularProducts() {
    if (!this.popularProductsCache) {
      this.popularProductsCache = this.productService
        .getProducts({ limit: 20, popular: 1, expand: 'largeImg' })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.popularProductsCache;
  }
  get reviews() {
    if (!this.reviewsCache) {
      this.reviewsCache = this.postService
        .getPosts({ category_slug: 'reviews', limit: 10, expand: 'content' })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.reviewsCache;
  }
}
