import { Component, OnInit, OnDestroy } from '@angular/core';
import { Media } from '../../share/inverfaces/media.int';
import { Subscription } from 'rxjs';
import { MediaService } from '../../share/services/media.service';

@Component({
  selector: 'ln-folio',
  templateUrl: './folio.component.html',
  styleUrls: ['./folio.component.scss']
})
export class FolioComponent implements OnInit, OnDestroy {
  images: Media[];
  isLoaded = false;
  sub: Subscription;
  constructor(private mediaService: MediaService) {}

  ngOnInit() {
    this.sub = this.mediaService.getAlbum('portfolio').subscribe(res => {
      this.images = res['data'];
      this.isLoaded = true;
    });
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
