import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiBase } from './base/api-base.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AppService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }

  getSettings(): Observable<any> {
    return this.get(`/settings`).pipe(map(res => res['data']));
  }
}
