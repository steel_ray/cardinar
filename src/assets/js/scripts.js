$(document).ready(function () {
  custom_cursor();
  $(document).on('mouseenter', '.fractured', function () {
    TweenMax.staggerTo($(this).find("span span"), 0.2, {
      x: 5,
      y: 5,
      autoAlpha: 0
    }, 0.05)
    TweenMax.staggerTo($(this).find("span span"), 0, {
      x: -5,
      y: -5,
      autoAlpha: 0,
      delay: 0.2,
    }, 0.05)
    TweenMax.staggerTo($(this).find("span span"), 0.2, {
      x: 0,
      y: 0,
      autoAlpha: 1,
      delay: 0.2
    }, 0.05)
  });
  $(window).scroll(function () {
    $('.check-visibility:not(.visible)').each(function () {
      var _this = $(this);
      if (checkVisible(_this)) {
        setTimeout(function () {
          _this.addClass('visible');
          initGridEffect();
        }, 1000);
      }
    });
  });
  $(document).on('click', '.scroll-down', function (e) {
    var top = $('.maru-new-items').offset().top;
    $('html,body').animate({
      scrollTop: top
    })
  });

  var moveForce = 30; // max popup movement in pixels
  var rotateForce = 20; // max popup rotation in deg

  $(document).mousemove(function (e) {
    var docX = $(document).width();
    var docY = $(document).height();

    var moveX = (e.pageX - docX / 2) / (docX / 2) * -moveForce;
    var moveY = (e.pageY - docY / 2) / (docY / 2) * -moveForce;

    var rotateY = (e.pageX / docX * rotateForce * 2) - rotateForce;
    var rotateX = -((e.pageY / docY * rotateForce * 2) - rotateForce);

    var target = $('.mouse-watch');

    target
      .css('left', moveX + 'px')
      .css('top', moveY + 'px')
      .css('transform', 'rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)');
  });
});


function initGridEffect() {
  $('.gridEffect').each(function (key, el) {
    const g = new InitGridLoaderFx(el);
  })
}

function checkVisible(elm, evalType) {
  evalType = evalType || "visible";

  var vpH = $(window).height(), // Viewport Height
    st = $(window).scrollTop(), // Scroll Top
    y = $(elm).offset().top,
    elementHeight = $(elm).height();

  if (evalType === "visible") return ((y < (vpH + st)) && (y > (st - elementHeight)));
  if (evalType === "above") return ((y < (vpH + st)));
}


function custom_cursor() {
  function mouseStopped() {
    node.removeClass("moving")
  }
  var timer, mouseX = 0,
    mouseY = 0,
    node_mouseX = 0,
    node_mouseY = 0,
    cursor_xp = 0,
    cursor_yp = 0,
    node_xp = 0,
    node_yp = 0,
    cursor = $("#cursor"),
    node = $("#node");
  $(document).mousemove(function (e) {
    cursor.addClass("moving"), node.addClass("moving"), mouseX = e.pageX - 3, mouseY = e.pageY - 3, node_mouseX = e.pageX - 8, node_mouseY = e.pageY - 8, clearTimeout(timer), timer = setTimeout(mouseStopped, 900), 0 != $("a:hover").length ? node.addClass("expand") : node.removeClass("expand")
  });
  $(document).on('click', '*', function (e) {
    cursor.addClass('clicked');
    setTimeout(function () {
      cursor.removeClass('clicked');
    }, 1000);
  });
  setInterval(function () {
    cursor_xp += (mouseX - cursor_xp) / 1, cursor_yp += (mouseY - cursor_yp) / 1, node_xp += (node_mouseX - node_xp) / 7, node_yp += (node_mouseY - node_yp) / 7, cursor.css({
      left: cursor_xp + "px",
      top: cursor_yp + "px"
    }), node.css({
      left: node_xp + "px",
      top: node_yp + "px"
    })
  }, 30);
}

var colors = ['#ccc', '#eb8200', '#ed2c2c'];

function animateMouseFollower(event) {
  var follower = document.createElement('div');

  follower.setAttribute('class', 'mouse-follower');
  document.body.appendChild(follower);

  follower.style.left = event.clientX + 'px';
  follower.style.top = event.clientY + 'px';

  var color = colors[Math.floor(Math.random() * colors.length)];
  follower.style.borderColor = color;

  follower.style.transition = 'all 0.5s linear 0s';

  follower.style.left = follower.offsetLeft - 20 + 'px';
  follower.style.top = follower.offsetTop - 20 + 'px';

  follower.style.width = '50px';
  follower.style.height = '50px';
  follower.style.borderWidth = '5px';
  follower.style.opacity = 0;
}

document.onscroll = fixFeedback;

function fixFeedback(e) {
  var target = document.getElementById('feedback-window'),
    targetOffsetTop = document.getElementById('header'),
    scrollTop = e.pageY;
  if (targetOffsetTop) {
    targetOffsetTop = targetOffsetTop.clientHeight;
    if (scrollTop >= targetOffsetTop) {
      target.classList.add('fixed');
    } else {
      target.classList.remove('fixed');
    }
  }

}

//declare variables
var ticking = false,
  decimalX = 0,
  decimalY = 0;

//Get the parent element, attach mousemove listener
var insideTarget = false;
document.onmousemove = function (e) {
  var niImgs = document.getElementsByClassName('__maru-new-items');
  insideTarget = false;
  if (niImgs.length) {
    niImgs[0].addEventListener('mousemove', cursorPositionHandler);
    if (e.pageY < niImgs[0].offsetTop || e.pageY > niImgs[0].offsetTop + niImgs[0].clientHeight) {
      TweenLite.to(document.getElementsByClassName('ni-imgs-cont'), 0.2, {
        rotationY: 0,
        rotationX: 0,
        ease: Power2.easeOut,
        transformPerspective: 500,
        transformOrigin: "center"
      });
    }
  }
}

//Declare mousemove handler
function cursorPositionHandler(ev) {
  //Calculate amount to transform (range fron -0.5 to 0.5)
  decimalX = ev.clientX / window.innerWidth - 0.5;
  decimalY = ev.clientY / window.innerHeight - 0.5;
  //Request animation frame
  requestTick();
}

function requestTick() {
  //Check not already rendering
  if (!ticking) {
    requestAnimationFrame(update);
    //Set status
    ticking = true;
  }
}

function update() {
  //Animate rotations
  TweenLite.to(document.getElementsByClassName('ni-imgs-cont'), 0.2, {
    rotationY: -25 * decimalX,
    rotationX: 25 * decimalY,
    ease: Power2.easeOut,
    transformPerspective: 500,
    transformOrigin: "center"
  });
  //Set status
  ticking = false;
}
